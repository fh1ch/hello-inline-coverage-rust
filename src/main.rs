fn run() {
	// With coverage
	if option_env!("NOT_SET") == None {
		println!("Will be executed")
	}
}

#[cfg(test)]
mod main {
  use super::*;

  #[test]
  fn test_run() {
    run()
  }
}

fn main() {
	run()
}
